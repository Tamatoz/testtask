package ru.stackprod.testtask;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import ru.stackprod.testtask.DI.Injector;

/**
 * Created by stackprod on 29.07.17.
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectDependencies();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearDependencies();
    }

    protected abstract void injectDependencies();
    protected abstract void clearDependencies();
}
