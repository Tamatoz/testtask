package ru.stackprod.testtask;

/**
 * Created by stackprod on 29.07.17.
 */

public class BasePresenter<T> {
    protected T activity;
    public BasePresenter(T activity){
        this.activity = activity;
    }
}
