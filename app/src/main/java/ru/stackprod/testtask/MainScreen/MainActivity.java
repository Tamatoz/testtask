package ru.stackprod.testtask.MainScreen;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.stackprod.testtask.BaseActivity;
import ru.stackprod.testtask.DI.Injector;
import ru.stackprod.testtask.R;

public class MainActivity extends BaseActivity {
    @Inject
    MainScreenPresenterInterface presenter;

    @BindView(R.id.currentWeatherButton)
    Button currentWeatherButton;

    @BindView(R.id.searchWeatherButton)
    Button searchWeatherButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @Override
    protected void injectDependencies() {
        Injector.plus(this);
        Injector.getMainScreenComponent().inject(this);
    }

    @Override
    protected void clearDependencies() {
        Injector.clear(this);
    }

    @OnClick(R.id.currentWeatherButton)
    public void currentWeather() {
        presenter.onCurrentWeatherClick();
    }

    @OnClick(R.id.searchWeatherButton)
    public void searchWeather() {
        presenter.onSearchWeatherClick();
    }

}
