package ru.stackprod.testtask.MainScreen;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.stackprod.testtask.CustomApplication;
import ru.stackprod.testtask.DI.Scopes.ActivityScope;


/**
 * Created by stackprod on 29.07.17.
 */

@Module
public class MainScreenModule {
    private MainActivity activity;

    public MainScreenModule(MainActivity activity){
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    MainScreenPresenterInterface provideMainScreenPresenter(){
        return new MainScreenPresenter(activity);
    }

}
