package ru.stackprod.testtask.MainScreen;

/**
 * Created by stackprod on 30.07.17.
 */

public interface MainScreenPresenterInterface {
    void onCurrentWeatherClick();
    void onSearchWeatherClick();
}
