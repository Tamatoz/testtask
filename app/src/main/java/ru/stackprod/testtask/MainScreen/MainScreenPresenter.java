package ru.stackprod.testtask.MainScreen;

import android.content.Intent;

import ru.stackprod.testtask.BasePresenter;
import ru.stackprod.testtask.CurrentWeatherScreen.CurrentWeatherActivity;
import ru.stackprod.testtask.SearchWeatherScreen.SearchWeatherActivity;

/**
 * Created by stackprod on 29.07.17.
 */

public class MainScreenPresenter extends BasePresenter<MainActivity> implements MainScreenPresenterInterface {

    public MainScreenPresenter(MainActivity activity) {
        super(activity);
    }

    @Override
    public void onCurrentWeatherClick() {
        activity.startActivity(new Intent(activity, CurrentWeatherActivity.class));
    }

    @Override
    public void onSearchWeatherClick() {
        activity.startActivity(new Intent(activity, SearchWeatherActivity.class));
    }
}
