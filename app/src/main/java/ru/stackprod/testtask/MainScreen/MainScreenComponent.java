package ru.stackprod.testtask.MainScreen;

import dagger.Subcomponent;
import ru.stackprod.testtask.DI.Scopes.ActivityScope;

/**
 * Created by stackprod on 29.07.17.
 */

@ActivityScope
@Subcomponent(modules = {MainScreenModule.class})
public interface MainScreenComponent {
    void inject(MainActivity activity);
}
