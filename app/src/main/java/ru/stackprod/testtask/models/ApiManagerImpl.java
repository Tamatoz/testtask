package ru.stackprod.testtask.models;

import android.content.Context;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import retrofit2.Retrofit;
import ru.stackprod.testtask.CustomApplication;
import ru.stackprod.testtask.DI.Injector;
import ru.stackprod.testtask.POJO.Conditions;
import ru.stackprod.testtask.POJO.ForecastDay;
import ru.stackprod.testtask.POJO.SearchResponse;
import ru.stackprod.testtask.R;

/**
 * Created by stackprod on 30.07.17.
 */

public class ApiManagerImpl implements ApiManager {
    Api api, autocompleteApi;

    @Inject
    ExceptionsClassificator exceptionsClassificator;

    public ApiManagerImpl(Api api, Api autocompleteApi){
        this.api = api;
        this.autocompleteApi = autocompleteApi;
        Injector.getAppComponent().inject(this);
    }

    @Override
    public Observable<List<ForecastDay>> getForecast(String coordinates) {

        return api.getSevenDaysForecast(coordinates).flatMap(
                new Function<JsonObject, Observable<List<ForecastDay>>>() {
                    @Override
                    public Observable<List<ForecastDay>> apply(@NonNull JsonObject jsonObject) throws Exception {
                        Gson gson = new Gson();
                        List<ForecastDay> forecastObjs = gson.fromJson(
                                jsonObject.get("forecast").getAsJsonObject()
                                        .get("txt_forecast").getAsJsonObject()
                                        .get("forecastday"), new TypeToken<List<ForecastDay>>() {
                                }.getType());
                        return Observable.fromArray(forecastObjs);
                    }
                }).onErrorResumeNext(throwable -> {
                    return Observable.error(exceptionsClassificator.classificate(throwable));
                });
    }

    @Override
    public Observable<Conditions> getCurrentConditions(String coordinates) {

        return api.getCurrentConditions(coordinates).flatMap(new Function<JsonObject, Observable<Conditions>>() {
            @Override
            public Observable<Conditions> apply(@NonNull JsonObject jsonObject) throws Exception {
                Gson gson = new Gson();
                Conditions conditions = gson.fromJson(
                        jsonObject.get("current_observation").getAsJsonObject(),
                        new TypeToken<Conditions>(){}.getType()
                );
                return Observable.fromArray(conditions);
            }
        }).onErrorResumeNext(throwable -> {
            return Observable.error(exceptionsClassificator.classificate(throwable));
        });
    }

    @Override
    public Observable<List<SearchResponse>> getCityAutocomplete(String city) {
        return autocompleteApi.getAutocompleteResults(city).flatMap(new Function<JsonObject, Observable<List<SearchResponse>>>() {
            @Override
            public Observable<List<SearchResponse>> apply(@NonNull JsonObject jsonObject) throws Exception {
                Gson gson = new Gson();
                List<SearchResponse> resultsList = gson.fromJson(
                        jsonObject.get("RESULTS").getAsJsonArray(),
                        new TypeToken<List<SearchResponse>>(){}.getType()
                );
                return Observable.fromArray(resultsList);
            }
        }).onErrorResumeNext(throwable -> {
            return Observable.error(exceptionsClassificator.classificate(throwable));
        });
    }
}
