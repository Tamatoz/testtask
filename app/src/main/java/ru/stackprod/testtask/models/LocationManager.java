package ru.stackprod.testtask.models;

import android.content.Context;
import android.location.Location;

import io.reactivex.Observable;

/**
 * Created by stackprod on 30.07.17.
 */

public interface LocationManager {
    Observable<Location> getLocation(Context context);
}
