package ru.stackprod.testtask.models;

import android.content.Context;
import android.widget.Toast;

import com.patloew.rxlocation.GoogleApiConnectionException;

import java.net.UnknownHostException;

import ru.stackprod.testtask.R;

/**
 * Created by stackprod on 01.08.17.
 */

public class ExceptionsClassificator {
    private Context context;

    public ExceptionsClassificator(Context c){
        context = c;
    }

    public Throwable classificate(Throwable throwable){
        throwable.printStackTrace();
        if (throwable instanceof UnknownHostException) {
            return new Throwable(context.getString(R.string.exception_connection));
        } else if (throwable instanceof GoogleApiConnectionException) {
            return new Throwable(context.getString(R.string.exception_googleapi));
        } else if (throwable instanceof NullPointerException) {
            return new Throwable(context.getString(R.string.exception_noresults));
        }else {
            return new Throwable(context.getString(R.string.exception_unknown));
        }
    }
}
