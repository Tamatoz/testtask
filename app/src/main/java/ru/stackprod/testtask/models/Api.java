package ru.stackprod.testtask.models;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import ru.stackprod.testtask.Constants;

/**
 * Created by stackprod on 30.07.17.
 */

public interface Api {
    @GET("api/" + Constants.WEATHER_API_KEY + "/forecast7day/q/{coordinates}.json")
    Observable<JsonObject> getSevenDaysForecast(@Path("coordinates") String coordinates);

    @GET("api/" + Constants.WEATHER_API_KEY + "/conditions/q/{coordinates}.json")
    Observable<JsonObject> getCurrentConditions(@Path("coordinates") String coordinates);

    @GET("aq")
    Observable<JsonObject> getAutocompleteResults(@Query("query") String query);
}
