package ru.stackprod.testtask.models;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.location.LocationRequest;
import com.patloew.rxlocation.RxLocation;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import ru.stackprod.testtask.DI.Injector;

/**
 * Created by stackprod on 30.07.17.
 */

public class LocationManagerImpl implements LocationManager {

    @Inject
    ExceptionsClassificator exceptionsClassificator;

    public LocationManagerImpl(){
        Injector.getAppComponent().inject(this);
    }

    @Override
    public Observable<Location> getLocation(Context context) {
        RxLocation rxLocation = new RxLocation(context);

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return null;
        }

        LocationRequest locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setNumUpdates(1)
                .setInterval(1000);
        return rxLocation.location().updates(locationRequest)
                .onErrorResumeNext(throwable -> {
            return Observable.error(exceptionsClassificator.classificate(throwable));
        });
    }

}
