package ru.stackprod.testtask.models;

import java.util.List;

import io.reactivex.Observable;
import ru.stackprod.testtask.POJO.Conditions;
import ru.stackprod.testtask.POJO.ForecastDay;
import ru.stackprod.testtask.POJO.SearchResponse;

/**
 * Created by stackprod on 30.07.17.
 */

public interface ApiManager {
    Observable<List<ForecastDay>> getForecast(String coordinates);

    Observable<Conditions> getCurrentConditions(String coordinates);

    Observable<List<SearchResponse>> getCityAutocomplete(String city);
}
