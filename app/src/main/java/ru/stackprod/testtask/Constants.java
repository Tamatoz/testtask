package ru.stackprod.testtask;

/**
 * Created by stackprod on 30.07.17.
 */

public class Constants {
    public static final String API_HOST = "http://api.wunderground.com/api/";
    public static final String AUTOCOMPLETE_HOST = "http://autocomplete.wunderground.com";
    public static final String WEATHER_API_KEY = "e40f555f90868350";

    public static final String API = "api";
    public static final String AUTOCOMPLETE = "autocomplete";
}
