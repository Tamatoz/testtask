package ru.stackprod.testtask.CurrentWeatherScreen;

import dagger.Subcomponent;
import ru.stackprod.testtask.DI.Scopes.ActivityScope;
import ru.stackprod.testtask.SearchWeatherScreen.SearchWeatherActivity;
import ru.stackprod.testtask.SearchWeatherScreen.SearchWeatherComponent;
import ru.stackprod.testtask.SearchWeatherScreen.SearchWeatherModule;

/**
 * Created by stackprod on 30.07.17.
 */

@ActivityScope
@Subcomponent(modules = {CurrentWeatherModule.class})
public interface CurrentWeatherComponent {
    void inject(CurrentWeatherActivity activity);

    SearchWeatherComponent plus(SearchWeatherModule module);
}
