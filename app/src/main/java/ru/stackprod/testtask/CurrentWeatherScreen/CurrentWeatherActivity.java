package ru.stackprod.testtask.CurrentWeatherScreen;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.stackprod.testtask.BaseActivity;
import ru.stackprod.testtask.DI.Injector;
import ru.stackprod.testtask.POJO.Conditions;
import ru.stackprod.testtask.POJO.ForecastDay;
import ru.stackprod.testtask.R;
import ru.stackprod.testtask.SearchWeatherScreen.SearchWeatherActivity;

/**
 * Created by stackprod on 30.07.17.
 */

public class CurrentWeatherActivity extends BaseActivity {

    @BindView(R.id.progressView)
    AVLoadingIndicatorView progressBar;

    @BindView(R.id.weatherImage)
    ImageView weatherImage;

    @BindView(R.id.locationName)
    TextView locationNameTextView;

    @BindView(R.id.weather)
    TextView weatherTextView;

    @BindView(R.id.temperature)
    TextView temperatureTextView;

    @BindView(R.id.relativeHumidity)
    TextView humidityTextView;

    @BindView(R.id.windInfo)
    TextView windTextView;

    @BindView(R.id.forecastList)
    ListView forecastListView;

    @BindView(R.id.parent)
    SwipeRefreshLayout parentSwipeLayout;

    ForecastAdapter adapter;

    @Inject
    CurrentWeatherPresenterInterface presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_weather);
        ButterKnife.bind(this);

        presenter.requestLocation();

        initRefreshLayout();
    }

    @Override
    protected void injectDependencies() {
        Injector.plus(this);
        Injector.getCurrentWeatherComponent().inject(this);
    }

    @Override
    protected void clearDependencies() {
        Injector.clear(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[],
                                           int[] grantResults) {
        if(requestCode == presenter.getLocationPermissionCode()) {
                if (grantResults.length > 1
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    presenter.permissionGranted();
                } else {
                    Toast.makeText(this, getText(R.string.exception_permissions_not_granted), Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }

    private void initRefreshLayout(){
        parentSwipeLayout.setOnRefreshListener(() -> {
            presenter.requestLocation();
        });
        forecastListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition =
                        (forecastListView == null || forecastListView.getChildCount() == 0) ?
                                0 : forecastListView.getChildAt(0).getTop();
                parentSwipeLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        return super.onCreateOptionsMenu(menu);
    }


    protected void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
        progressBar.show();
    }

    protected void hideLoading(){
        progressBar.hide();
        parentSwipeLayout.setRefreshing(false);
    }

    protected void fillForecast(List<ForecastDay> forecastObjs){
        if(adapter == null){
            adapter = new ForecastAdapter(this, R.layout.item_forecast, forecastObjs);
            forecastListView.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    protected void fillConditions(Conditions conditions){
        if(conditions.getIconUrl() != null){
            Picasso.with(this).load(conditions.getIconUrl()).into(weatherImage);
        }
        locationNameTextView.setText(conditions.getLocationInfo().getFullName());
        weatherTextView.setText(conditions.getWeather());
        temperatureTextView.setText(conditions.getTemperature());
        humidityTextView.setText(String.format(
                "%s:  %s",
                getString(R.string.conditions_info_humidity),
                conditions.getRelativeHumidity())
        );
        windTextView.setText(String.format(
                "%s:  %s, %s, %d kph (%d mph)",
                getString(R.string.conditions_info_wind),
                conditions.getWind(),
                conditions.getWindDirection(),
                conditions.getWindSpeedKph(),
                conditions.getWindSpeedMph())
        );
    }
}
