package ru.stackprod.testtask.CurrentWeatherScreen;

/**
 * Created by stackprod on 30.07.17.
 */

public interface CurrentWeatherPresenterInterface {

    int getLocationPermissionCode();

    void permissionGranted();

    void requestLocation();
}
