package ru.stackprod.testtask.CurrentWeatherScreen;

import android.Manifest;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.patloew.rxlocation.GoogleApiConnectionException;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import ru.stackprod.testtask.BasePresenter;
import ru.stackprod.testtask.DI.Injector;
import ru.stackprod.testtask.POJO.ForecastDay;
import ru.stackprod.testtask.R;
import ru.stackprod.testtask.SearchWeatherScreen.SearchWeatherActivity;
import ru.stackprod.testtask.models.ApiManager;
import ru.stackprod.testtask.models.LocationManager;

/**
 * Created by stackprod on 30.07.17.
 */

public class CurrentWeatherPresenter extends BasePresenter<CurrentWeatherActivity>
        implements CurrentWeatherPresenterInterface {

    private static final int LOCATION_PERMISSION_CODE = 100;

    @Inject
    LocationManager locationManager;

    @Inject
    protected ApiManager apiManager;

    List<ForecastDay> forecastObjs = new ArrayList<>();

    public CurrentWeatherPresenter(final CurrentWeatherActivity activity) {
        super(activity);
        Injector.getAppComponent().inject(this);
    }

    @Override
    public int getLocationPermissionCode() {
        return LOCATION_PERMISSION_CODE;
    }

    @Override
    public void permissionGranted() {
        requestLocation();
    }

    @Override
    public void requestLocation(){
        if(activity instanceof SearchWeatherActivity)
            return;

        if(locationManager.getLocation(activity) == null){
            ActivityCompat.requestPermissions(activity,
                    new String[]{
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION
                    },
                    LOCATION_PERMISSION_CODE);
        } else {
            activity.showLoading();

            locationManager.getLocation(activity).subscribe(
                    location -> {
                        activity.hideLoading();
                        getCurrentConditions(
                                Double.toString(location.getLatitude()) + "," + location.getLongitude()
                        );
                    },
                    throwable -> {
                        activity.hideLoading();
                        Toast.makeText(
                                activity,
                                throwable.getMessage(),
                                Toast.LENGTH_SHORT
                        ).show();
                    });
        }
    }

    private void getForecast(String coordinates){
        activity.showLoading();

        apiManager.getForecast(coordinates)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        forecastDays -> {
                            fillForecastList(forecastDays);
                            activity.fillForecast(forecastObjs);
                        },
                        throwable -> {
                            activity.hideLoading();
                            Toast.makeText(
                                    activity,
                                    throwable.getMessage(),
                                    Toast.LENGTH_SHORT
                            ).show();
                        }, () -> {
                            activity.hideLoading();
                        }
                );
    }

    protected void getCurrentConditions(String coordinates){
        activity.showLoading();
        apiManager.getCurrentConditions(coordinates)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        conditions -> {
                            activity.fillConditions(conditions);
                            getForecast(coordinates);
                        },
                        throwable -> {
                            activity.hideLoading();
                            Toast.makeText(
                                    activity,
                                    throwable.getMessage(),
                                    Toast.LENGTH_SHORT
                            ).show();
                        },
                        () -> {
                                activity.hideLoading();
                        }

                );
    }

    private void fillForecastList(List<ForecastDay> objs){
        forecastObjs.clear();
        forecastObjs.addAll(objs);
    }
}
