package ru.stackprod.testtask.CurrentWeatherScreen;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.stackprod.testtask.POJO.ForecastDay;
import ru.stackprod.testtask.R;

/**
 * Created by stackprod on 31.07.17.
 */

public class ForecastAdapter extends ArrayAdapter {

    private List<ForecastDay> forecastObjs;
    private Context context;
    private int resource;

    public ForecastAdapter(Context context, int resource, List<ForecastDay> forecastObjs) {
        super(context, resource, forecastObjs);
        this.forecastObjs = forecastObjs;
        this.context = context;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = View.inflate(context, resource, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }
        holder.nameTextView.setText(forecastObjs.get(position).getTitle());
        holder.forecastTextView.setText(forecastObjs.get(position).getForecast());
        if(forecastObjs.get(position).getIconUrl() != null)
            Picasso.with(context)
                    .load(forecastObjs.get(position).getIconUrl())
                    .into(holder.forecastImageView);

        view.setBackgroundColor(
                forecastObjs.get(position).getPeriod() % 2 == 1 ?
                        ContextCompat.getColor(context, R.color.colorSecondHalfOfDay) :
                        ContextCompat.getColor(context, R.color.colorFirstHalfOfDay)
        );



        return view;
    }

    static class ViewHolder {
        @BindView(R.id.forecastName)
        TextView nameTextView;
        @BindView(R.id.forecast) TextView forecastTextView;
        @BindView(R.id.forecastImage)
        ImageView forecastImageView;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
