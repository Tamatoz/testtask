package ru.stackprod.testtask.CurrentWeatherScreen;

import dagger.Module;
import dagger.Provides;
import ru.stackprod.testtask.DI.Scopes.ActivityScope;
import ru.stackprod.testtask.MainScreen.MainActivity;
import ru.stackprod.testtask.MainScreen.MainScreenPresenter;
import ru.stackprod.testtask.MainScreen.MainScreenPresenterInterface;

/**
 * Created by stackprod on 30.07.17.
 */

@Module
public class CurrentWeatherModule {
    private CurrentWeatherActivity activity;

    public CurrentWeatherModule(CurrentWeatherActivity activity){
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    CurrentWeatherPresenterInterface provideCurrentWeatherPresenter(){
        return new CurrentWeatherPresenter(activity);
    }
}
