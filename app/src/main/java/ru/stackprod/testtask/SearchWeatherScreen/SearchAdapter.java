package ru.stackprod.testtask.SearchWeatherScreen;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.stackprod.testtask.CurrentWeatherScreen.ForecastAdapter;
import ru.stackprod.testtask.POJO.SearchResponse;
import ru.stackprod.testtask.R;

/**
 * Created by stackprod on 01.08.17.
 */

public class SearchAdapter extends ArrayAdapter {

    List<SearchResponse> responseList;
    Context context;
    int resource;
    public SearchAdapter(Context context, int resource, List<SearchResponse> responseList) {
        super(context, resource, responseList);
        this.context = context;
        this.resource = resource;
        this.responseList = responseList;
    }

    @NonNull
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = View.inflate(context, resource, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }
        holder.name.setText(responseList.get(position).getName());
        return view;
    }

    static class ViewHolder {
        @BindView(android.R.id.text1)
        TextView name;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
