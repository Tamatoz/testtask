package ru.stackprod.testtask.SearchWeatherScreen;

import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.jakewharton.rxbinding2.widget.TextViewTextChangeEvent;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;
import ru.stackprod.testtask.BasePresenter;
import ru.stackprod.testtask.CurrentWeatherScreen.CurrentWeatherActivity;
import ru.stackprod.testtask.CurrentWeatherScreen.CurrentWeatherPresenter;
import ru.stackprod.testtask.POJO.SearchResponse;

/**
 * Created by stackprod on 01.08.17.
 */

public class SearchWeatherPresenter extends CurrentWeatherPresenter implements SearchWeatherPresenterInterface{

    private static final long DELAY_IN_MILLIS = 600;
    List<SearchResponse> searchResponseList;

    public SearchWeatherPresenter(SearchWeatherActivity activity) {
        super(activity);
    }

    @Override
    public void observeTextChange(AutoCompleteTextView autoCompleteTextView) {
        autoCompleteTextView.setOnItemClickListener((parent, view, position, id) -> {
            autoCompleteTextView.setText(searchResponseList.get(position).getName());
            getWeatherBySearchResponse(searchResponseList.get(position));
        });

        autoCompleteTextView.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if(searchResponseList != null &&
                        searchResponseList.size() > 0 &&
                        searchResponseList.get(0).getName().equals(autoCompleteTextView.getText().toString())){
                    getWeatherBySearchResponse(searchResponseList.get(0));
                }
                return true;
            }
            return false;
        });

        Observable<List<SearchResponse>> autocompleteResponseObservable =
                RxTextView.textChangeEvents(autoCompleteTextView)
                        .debounce(DELAY_IN_MILLIS, TimeUnit.MILLISECONDS)
                        .map(textViewTextChangeEvent -> textViewTextChangeEvent.text().toString())
                        .filter(s -> s.length() >= 2)
                        .observeOn(Schedulers.io())
                        .flatMap(new Function<String, Observable<List<SearchResponse>>>() {
                            @Override
                            public Observable<List<SearchResponse>> apply(String s) {
                                return apiManager.getCityAutocomplete(s);
                            }
                        })
                        .observeOn(AndroidSchedulers.mainThread());

        autocompleteResponseObservable
                .subscribe(searchResponses -> {
                    searchResponseList = searchResponses;
                    SearchAdapter adapter = new SearchAdapter(
                            activity,
                            android.R.layout.simple_list_item_1,
                            searchResponses
                    );
                    autoCompleteTextView.setAdapter(adapter);
                    String enteredText = autoCompleteTextView.getText().toString();
                    if (searchResponses.size() >= 1 && enteredText.equals(searchResponses.get(0).getName())) {
                        autoCompleteTextView.dismissDropDown();
                    } else {
                        autoCompleteTextView.showDropDown();
                    }
                });
    }

    private void getWeatherBySearchResponse(SearchResponse response){
        getCurrentConditions(
                String.format(
                        "%s,%s",
                        response.getLat(),
                        response.getLon()
                )
        );
    }


}
