package ru.stackprod.testtask.SearchWeatherScreen;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;

import javax.inject.Inject;

import butterknife.ButterKnife;
import ru.stackprod.testtask.CurrentWeatherScreen.CurrentWeatherActivity;
import ru.stackprod.testtask.DI.Injector;
import ru.stackprod.testtask.R;

/**
 * Created by stackprod on 01.08.17.
 */

public class SearchWeatherActivity extends CurrentWeatherActivity {

    @Inject
    SearchWeatherPresenterInterface presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    protected void injectDependencies() {
        super.injectDependencies();
        Injector.plus(this);
        Injector.getSearchWeatherComponent().inject(this);
    }

    @Override
    protected void clearDependencies() {
        Injector.clear(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);

        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.view_action_bar, null);

        actionBar.setCustomView(v);

        AutoCompleteTextView searchView = ButterKnife.findById(v, R.id.searchView);
        presenter.observeTextChange(searchView);

        return super.onCreateOptionsMenu(menu);
    }
}
