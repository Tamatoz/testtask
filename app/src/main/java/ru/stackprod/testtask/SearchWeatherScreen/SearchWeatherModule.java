package ru.stackprod.testtask.SearchWeatherScreen;

import dagger.Module;
import dagger.Provides;
import ru.stackprod.testtask.CurrentWeatherScreen.CurrentWeatherActivity;
import ru.stackprod.testtask.CurrentWeatherScreen.CurrentWeatherPresenter;
import ru.stackprod.testtask.CurrentWeatherScreen.CurrentWeatherPresenterInterface;
import ru.stackprod.testtask.DI.Scopes.ActivityScope;
import ru.stackprod.testtask.DI.Scopes.SearchScreenScope;

/**
 * Created by stackprod on 01.08.17.
 */

@Module
public class SearchWeatherModule {
    private SearchWeatherActivity activity;

    public SearchWeatherModule(SearchWeatherActivity activity){
        this.activity = activity;
    }

    @Provides
    @SearchScreenScope
    SearchWeatherPresenterInterface provideSearchWeatherPresenter(){
        return new SearchWeatherPresenter(activity);
    }
}
