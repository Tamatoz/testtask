package ru.stackprod.testtask.SearchWeatherScreen;

import android.widget.AutoCompleteTextView;

/**
 * Created by stackprod on 01.08.17.
 */

public interface SearchWeatherPresenterInterface {
    void observeTextChange(AutoCompleteTextView autoCompleteTextView);
}
