package ru.stackprod.testtask.SearchWeatherScreen;

import javax.inject.Singleton;

import dagger.Subcomponent;
import ru.stackprod.testtask.CurrentWeatherScreen.CurrentWeatherActivity;
import ru.stackprod.testtask.DI.Scopes.ActivityScope;
import ru.stackprod.testtask.DI.Scopes.SearchScreenScope;

/**
 * Created by stackprod on 01.08.17.
 */
@SearchScreenScope
@Subcomponent(modules = {SearchWeatherModule.class})
    public interface SearchWeatherComponent {
        void inject(SearchWeatherActivity activity);
}
