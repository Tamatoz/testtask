package ru.stackprod.testtask;

import android.app.Application;
import android.content.Context;

import javax.inject.Inject;

import ru.stackprod.testtask.DI.AppComponent;
import ru.stackprod.testtask.DI.AppModule;
import ru.stackprod.testtask.DI.Injector;
import ru.stackprod.testtask.MainScreen.MainScreenModule;

/**
 * Created by stackprod on 29.07.17.
 */

public class CustomApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Injector.init(this);
    }

}
