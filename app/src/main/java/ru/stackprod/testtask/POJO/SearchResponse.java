package ru.stackprod.testtask.POJO;

/**
 * Created by stackprod on 01.08.17.
 */

public class SearchResponse {

    private String name;

    private String lat;

    private String lon;

    public String getLon() {
        return lon;
    }

    public String getName() {
        return name;
    }

    public String getLat() {
        return lat;
    }
}
