package ru.stackprod.testtask.POJO;

import com.google.gson.annotations.SerializedName;

/**
 * Created by stackprod on 30.07.17.
 */

public class ForecastDay {

    private int period;

    private String title;

    @SerializedName("fcttext_metric")
    private String forecast;

    @SerializedName("icon_url")
    private String iconUrl;

    public String getIconUrl() {
        return iconUrl;
    }

    public int getPeriod() {
        return period;
    }

    public String getTitle() {
        return title;
    }

    public String getForecast() {
        return forecast;
    }

}
