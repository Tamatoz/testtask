package ru.stackprod.testtask.POJO;

import com.google.gson.annotations.SerializedName;

/**
 * Created by stackprod on 31.07.17.
 */

public class LocationInfo {

    @SerializedName("full")
    private String fullName;

    public String getFullName() {
        return fullName;
    }

}
