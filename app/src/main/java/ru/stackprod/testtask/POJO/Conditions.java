package ru.stackprod.testtask.POJO;

import com.google.gson.annotations.SerializedName;

/**
 * Created by stackprod on 31.07.17.
 */

public class Conditions {

    @SerializedName("display_location")
    private LocationInfo locationInfo;

    private String weather;

    @SerializedName("temperature_string")
    private String temperature;

    @SerializedName("relative_humidity")
    private String relativeHumidity;

    @SerializedName("wind_string")
    private String wind;

    @SerializedName("wind_dir")
    private String windDirection;

    @SerializedName("feelslike_string")
    private String feelsLike;

    @SerializedName("icon_url")
    private String iconUrl;

    @SerializedName("wind_kph")
    private int windSpeedKph;

    @SerializedName("wind_mph")
    private int windSpeedMph;

    public String getWeather() {
        return weather;
    }

    public String getTemperature() {
        return temperature;
    }

    public String getRelativeHumidity() {
        return relativeHumidity;
    }

    public String getWind() {
        return wind;
    }

    public String getWindDirection() {
        return windDirection;
    }

    public String getFeelsLike() {
        return feelsLike;
    }

    public int getWindSpeedKph() {
        return windSpeedKph;
    }

    public int getWindSpeedMph() {
        return windSpeedMph;
    }

    public LocationInfo getLocationInfo() {
        return locationInfo;
    }

    public String getIconUrl() {
        return iconUrl;
    }


}
