package ru.stackprod.testtask.DI;

import android.content.Context;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.stackprod.testtask.Constants;
import ru.stackprod.testtask.CustomApplication;
import ru.stackprod.testtask.models.Api;
import ru.stackprod.testtask.models.ApiManager;
import ru.stackprod.testtask.models.ApiManagerImpl;
import ru.stackprod.testtask.models.ExceptionsClassificator;

/**
 * Created by stackprod on 30.07.17.
 */

@Module
public class ApiModule {

    @Provides
    @Named(Constants.API)
    @Singleton
    Api provideApi(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.API_HOST + Constants.WEATHER_API_KEY + "/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
                .build();
        return retrofit.create(Api.class);
    }

    @Provides
    @Named(Constants.AUTOCOMPLETE)
    @Singleton
    Api provideAutocompleteApi(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.AUTOCOMPLETE_HOST + "/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
                .build();
        return retrofit.create(Api.class);
    }

    @Provides
    @Singleton
    ApiManager provideApiManager(){
        return new ApiManagerImpl(provideApi(), provideAutocompleteApi());
    }

    @Provides
    @Singleton
    ExceptionsClassificator provideExceptionsClassificator(Context context){
        return new ExceptionsClassificator(context);
    }
}
