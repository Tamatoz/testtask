package ru.stackprod.testtask.DI.Scopes;

import javax.inject.Scope;

/**
 * Created by stackprod on 29.07.17.
 */

@Scope
public @interface ActivityScope {
}
