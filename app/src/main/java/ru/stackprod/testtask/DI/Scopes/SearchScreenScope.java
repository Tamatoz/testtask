package ru.stackprod.testtask.DI.Scopes;

import javax.inject.Scope;

/**
 * Created by stackprod on 01.08.17.
 */

@Scope
public @interface SearchScreenScope {
}