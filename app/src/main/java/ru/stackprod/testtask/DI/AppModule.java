package ru.stackprod.testtask.DI;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.stackprod.testtask.CustomApplication;

/**
 * Created by stackprod on 29.07.17.
 */

@Module
public class AppModule {
    private CustomApplication app;

    public AppModule(CustomApplication application){
        this.app = application;
    }

    @Provides
    @Singleton
    Context provideApplication(){
        return app;
    }
}
