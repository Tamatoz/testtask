package ru.stackprod.testtask.DI;

import javax.inject.Singleton;

import dagger.Component;
import ru.stackprod.testtask.CurrentWeatherScreen.CurrentWeatherComponent;
import ru.stackprod.testtask.CurrentWeatherScreen.CurrentWeatherModule;
import ru.stackprod.testtask.CurrentWeatherScreen.CurrentWeatherPresenter;
import ru.stackprod.testtask.CurrentWeatherScreen.ForecastAdapter;
import ru.stackprod.testtask.CustomApplication;
import ru.stackprod.testtask.MainScreen.MainScreenComponent;
import ru.stackprod.testtask.MainScreen.MainScreenModule;
import ru.stackprod.testtask.SearchWeatherScreen.SearchWeatherComponent;
import ru.stackprod.testtask.SearchWeatherScreen.SearchWeatherModule;
import ru.stackprod.testtask.SearchWeatherScreen.SearchWeatherPresenter;
import ru.stackprod.testtask.models.ApiManagerImpl;
import ru.stackprod.testtask.models.LocationManagerImpl;

/**
 * Created by stackprod on 29.07.17.
 */

@Component(modules = {AppModule.class, LocationModule.class, ApiModule.class})
@Singleton
public interface AppComponent {
    void inject(CustomApplication app);
    void inject(CurrentWeatherPresenter currentWeatherPresenter);
    void inject(ApiManagerImpl apiManager);
    void inject(LocationManagerImpl locationManager);

    MainScreenComponent plus(MainScreenModule module);
    CurrentWeatherComponent plus(CurrentWeatherModule module);
}
