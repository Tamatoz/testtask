package ru.stackprod.testtask.DI;

import ru.stackprod.testtask.CurrentWeatherScreen.CurrentWeatherActivity;
import ru.stackprod.testtask.CurrentWeatherScreen.CurrentWeatherComponent;
import ru.stackprod.testtask.CurrentWeatherScreen.CurrentWeatherModule;
import ru.stackprod.testtask.CustomApplication;
import ru.stackprod.testtask.MainScreen.MainActivity;
import ru.stackprod.testtask.MainScreen.MainScreenComponent;
import ru.stackprod.testtask.MainScreen.MainScreenModule;
import ru.stackprod.testtask.SearchWeatherScreen.SearchWeatherActivity;
import ru.stackprod.testtask.SearchWeatherScreen.SearchWeatherComponent;
import ru.stackprod.testtask.SearchWeatherScreen.SearchWeatherModule;

/**
 * Created by stackprod on 29.07.17.
 */

public class Injector {
    private static AppComponent appComponent;

    private static MainScreenComponent mainScreenComponent;
    private static CurrentWeatherComponent currentWeatherComponent;
    private static SearchWeatherComponent searchWeatherComponent;

    public static void init(CustomApplication app){
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(app))
                .build();
        appComponent.inject(app);
    }

    public static MainScreenComponent plus(MainActivity activity){
        mainScreenComponent = appComponent.plus(new MainScreenModule(activity));
        return mainScreenComponent;
    }

    public static void clear(MainActivity activity){
        mainScreenComponent = null;
    }

    public static CurrentWeatherComponent plus(CurrentWeatherActivity activity){
        currentWeatherComponent = appComponent.plus(new CurrentWeatherModule(activity));
        return currentWeatherComponent;
    }

    public static void clear(CurrentWeatherActivity activity){
        currentWeatherComponent = null;
    }

    public static SearchWeatherComponent plus(SearchWeatherActivity activity){
        searchWeatherComponent = currentWeatherComponent.plus(new SearchWeatherModule(activity));
        return searchWeatherComponent;
    }

    public static void clear(SearchWeatherActivity activity){
        searchWeatherComponent = null;
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    public static MainScreenComponent getMainScreenComponent() {
        return mainScreenComponent;
    }

    public static CurrentWeatherComponent getCurrentWeatherComponent() {
        return currentWeatherComponent;
    }

    public static SearchWeatherComponent getSearchWeatherComponent() {
        return searchWeatherComponent;
    }
}
