package ru.stackprod.testtask.DI;

import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.stackprod.testtask.CustomApplication;
import ru.stackprod.testtask.models.LocationManager;
import ru.stackprod.testtask.models.LocationManagerImpl;

/**
 * Created by stackprod on 30.07.17.
 */

@Module
public class LocationModule {
    @Provides
    @NonNull
    @Singleton
    LocationManager provideLocationManager(){
        return new LocationManagerImpl();
    }
}
